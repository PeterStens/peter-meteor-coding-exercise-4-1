/*****************************************************************************/
/* Play: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Play.events ({

  'click #playerResponseButton': function (event, template) { //This helped me: http://stackoverflow.com/questions/28034512/dynamic-radio-button-creation-using-handlebars-in-meteor
    event.preventDefault ();
    var playerResponseCode = $ (":radio[name=playerResponsesRadio]:checked").val ();
    console.log ('playerResponseCode: ' + playerResponseCode);

    Meteor.call ('/app/processPlay', Session.get ('SCENARIO'), Session.get ('NPCHARACTER'), Session.get ('NPCHARACTER_REQUEST'), playerResponseCode, function (err, response) {
      console.log (JSON.stringify (response));
      Session.set ('result', JSON.stringify (response));

      var score = 0;

      if (Meteor.user().profile.score != undefined) {
        score = Meteor.user().profile.score;

      }

      Session.set('USER_SCORE',score);

      if (response.playResult != null) {

        alert (response.playResult);
      }

      // if new scenario, set new scenario
      // if new npCharacterRequest, set new npCharacterRequest
      // newScenario:null,newNPCharacter:null,newNPCharacterRequest:null
      if (response.newNPCharacterRequest != null) {
        Template.Play.setScenario (Session.get ('SCENARIO'), Session.get ('NPCHARACTER'), response.newNPCharacterRequest);
      }

      if (response.randomScenario === true) {

        Template.Play.randomScenario ();
      }

      if (err) {
        alert (err);
      }
    });
  }
});

Template.Play.helpers ({
  scenario: function () {

    var scenario = Session.get ('SCENARIO');
    return scenario;
  },
  npCharacter: function () {

    var character = Session.get ('NPCHARACTER');
    return character;
  },
  npCharacterRequest: function () {

    var request = Session.get ('NPCHARACTER_REQUEST');
    return request;

  },
  playerResponses: function () {

    var responses = Session.get ('PLAYER_RESPONSES');
    return responses;
  },
  userScore: function () {

    return Session.get ('USER_SCORE') ;

  },
  playResult: function () {

    return Session.get ('PLAY_RESULT') ;

  }
});

// Basically as the game plays
// There's going to be a counter
// The counter increments per interaction
// The counter will determine who you talk to
// And what you talk about
// Potentially each NPCharacter will have their own counter
// That way if you encounter the same NPCharacter, it will choose the next thing.
// The objective is to steal everything from the store that you can while not looking suspicious
// Talk to NPC's to lower suspicion.
// Opportunity's to steal are random and will move you to the next part of the store until you enter the checkout line
// The check out line will direct you to the cop and he will either arrest you or not.


Template.Play.randomScenario = function () {
  // Random Scenario
  //This line is fine, it's finding all of the Scenarios
  var scenarios = Scenario.find ({}).fetch ();

//  var randomIndex = 2; //Math.floor ((Math.random () * scenarios.length));
  var scenarioIndex = 0;

//  var randomScenario = scenarios[randomIndex];

  
  var gameScenario = scenarios[scenarioIndex];
  
  scenarioIndex++;
  
  console.log ('gameScenario: ' + JSON.stringify (gameScenario));

  // Random Character based on found scenario
  var npCharacters = NPCharacter.find ({scenarioCodes: gameScenario.objectCode}).fetch ();

  console.log ('npCharacters: ' + JSON.stringify (npCharacters));

  //var randomIndex = 0; //Math.floor ((Math.random () * npCharacters.length));

  // Character is chosen based on above^^
  var characterIndex = 0;
  var randomCharacter = npCharacters [characterIndex];
  
  // Character is created
  console.log ('randomCharacter: ' + JSON.stringify (randomCharacter));
  
  // NPCharacter's request pool is generated
  var requests = NPCharacterRequest.find ({objectCode: {$in: randomCharacter.npCharacterRequestCodes}}).fetch ();

  console.log ('requests: ' + JSON.stringify (requests));

  //var randomIndex = 0; //Math.floor ((Math.random () * requests.length));

  // NPCharacter's request is generated from the pool
  var requestIndex = 0;
  var randomRequest = requests[requestIndex];

  // NPCharacter says words
  console.log ('randomRequest: ' + JSON.stringify (randomRequest));
  
  // Scenario is put together based on generated things from above
  Template.Play.setScenario (gameScenario, randomCharacter, randomRequest);

};

Template.Play.setScenario = function (scenario, character, request) {

  Session.set ('SCENARIO', scenario);
  Session.set ('NPCHARACTER', character);
  Session.set ('NPCHARACTER_REQUEST', request);

  var playerResponses = PlayerResponse.find ({npCharacterRequestCodes: request.objectCode}).fetch ();

  Session.set ('PLAYER_RESPONSES', playerResponses);

  console.log ('playerResponses: ' + JSON.stringify (playerResponses));
}

/*****************************************************************************/
/* Play: Lifecycle Hooks */
/*****************************************************************************/
Template.Play.created = function () {

  var score = 0;

  if (Meteor.user().profile.score != undefined) {
    score = Meteor.user().profile.score;

  }

  Session.set('USER_SCORE',score);

  Template.Play.randomScenario ();

};

Template.Play.rendered = function () {


};

Template.Play.destroyed = function () {
};